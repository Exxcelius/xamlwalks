﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MbientLab.MetaWear;
using Windows.Devices.Bluetooth;

namespace XamlWalks
{
    public class DeviceRepresentation
    {
        public string name { get; private set; }
        public string Alias { get; private set; }
        public string MacAddressString { get; private set; }
        private ulong MacAddress;
        public BluetoothLEDevice device;

        public DeviceRepresentation() { }
        public DeviceRepresentation(BluetoothLEDevice bluetoothLEDevice)
        {
            if (bluetoothLEDevice != null)
            {
                this.device = bluetoothLEDevice;
                this.MacAddress = this.device.BluetoothAddress;
                this.name = this.device.Name;
                this.Alias = this.name;
                this.MacAddressString = MacConverter(this.MacAddress);
            }
        }

        public IMetaWearBoard GetMetaWearBoard() 
        {
            if (this.device != null)
            {
                return MbientLab.MetaWear.Win10.Application.GetMetaWearBoard(this.device);
            }


            return null;
        }

        private string MacConverter(ulong address)
        {
            string hexString = ((ulong)address).ToString("X");
            return hexString.Insert(2, ":").Insert(5, ":").Insert(8, ":").Insert(11, ":").Insert(14, ":");
        }

    }
}
