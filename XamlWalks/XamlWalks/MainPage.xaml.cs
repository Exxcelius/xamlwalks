﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Die Elementvorlage "Leere Seite" wird unter https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x407 dokumentiert.

namespace XamlWalks
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private DeviceWatcher watcher;
        
        private ObservableCollection<DeviceRepresentation> devices = new ObservableCollection<DeviceRepresentation>();

        private HashSet<string> seenDevices = new HashSet<string>();
        private Dictionary<string, DeviceRepresentation> connectedDevices = new Dictionary<string, DeviceRepresentation>();


        //private HashSet<ulong> seenDevices = new HashSet<ulong>();
        private int ScanDuration = 10000;
        private Timer timer;

        public MainPage()
        {
            this.InitializeComponent();

            pairedDevices.ItemsSource = devices;

            // setup Device Watcher
            string[] requestedProperties = { "System.Devices.Aep.DeviceAddress", "System.Devices.Aep.IsConnected" };
            watcher = DeviceInformation.CreateWatcher(BluetoothLEDevice.GetDeviceSelectorFromPairingState(false),
                                                      requestedProperties,
                                                      DeviceInformationKind.AssociationEndpoint);

            // Register event handlers before starting the watcher.
            // Added, Updated and Removed are required to get all nearby devices
            watcher.Added += async (s, o) =>
            {
                var keys = o.Properties.Keys;
                var info = o.Properties.Values;
                seenDevices.Add(o.Id);

                if (o.Name.Contains("MetaWear", StringComparison.InvariantCultureIgnoreCase))
                {
                    BluetoothLEDevice dev = await BluetoothLEDevice.FromIdAsync(o.Id);
                    var rep = new DeviceRepresentation(dev);
                    connectedDevices.Add(o.Id, rep);
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => devices.Add(rep));
                }   
            };

            //required Method, empty because updates are observable on stored BluetoothLEDevices (DeviceRepresentation.device)
            watcher.Updated += (s, o) => {};

            watcher.Removed += async (s, o) =>
            {
                this.connectedDevices.Remove(o.Id, out var rep);
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => devices.Remove(rep));
                this.seenDevices.Remove(o.Id);
            };

            // Start the watcher.
            if (watcher.Status != DeviceWatcherStatus.Started)
            {
                watcher.Start();
            }
        }

        private void pairedDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            Frame.Navigate(typeof(SensorPage), e.AddedItems[0] as DeviceRepresentation);


        }

        private void refreshDevices_Click(object sender, RoutedEventArgs args)
        {
            if (timer != null)
            {
                timer.Dispose();
                timer = null;
            }
            watcher.Stop();

            string[] requestedProperties = { "System.Devices.Aep.DeviceAddress", "System.Devices.Aep.IsConnected" };
            watcher = DeviceInformation.CreateWatcher(BluetoothLEDevice.GetDeviceSelectorFromPairingState(false),
                                                      requestedProperties,
                                                      DeviceInformationKind.AssociationEndpoint);

            devices.Clear();
            seenDevices.Clear();
            connectedDevices.Clear();
            watcher.Start();
            timer = new Timer(e => watcher.Stop(), null, ScanDuration, Timeout.Infinite);
        }

    }
}
