﻿using System;
using System.Collections;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using MbientLab.MetaWear;
using MbientLab.MetaWear.Sensor;
using MbientLab.MetaWear.Core;
using MbientLab.MetaWear.Data;

namespace XamlWalks
{
    public class Sensor
    {
        private ContentDialog exceptionPopup = new ContentDialog()
        {
            Title = "Fehlermitteilung",
            CloseButtonText = "OK"
        };

        //private DeviceRepresentation device;
        public DeviceRepresentation Device { get; set; }
        //string acquisitionMode;
        public bool MessStatus { get; set; }
        public string AcquisitionMode { get; set; }
        //private SensorStatus status;
        public SensorStatus Status { get; set; }
        //private SensorMask sensorChoosen = SensorMask.None;
        public SensorMask SensorChoosen { get; set; }
        public enum SensorStatus { Nichtverbunden, Verbunden, Bereit, Gestartet, Gestoppt, Download, Wartend };
        public enum SensorMask
        {
            None = 0b_0000_0000,  // 0
            ACCELEROMETER = 0b_0000_0001,  // 1
            GYROSCOPE = 0b_0000_0010,  // 2
            QUATERNION = 0b_0000_0100,  // 4
            EULER_ANGLE = 0b_0000_1000,  // 8
            LINEAR_ACCELERATION = 0b_0001_0000,  // 16
            GRAVITY = 0b_0010_0000,  // 32
            MAGNETOMETER = 0b_0100_0000,  // 64
        };

        //private mwbWrapper metawear;
        private IMetaWearBoard metawear;

        private Queue<Acceleration> valuesACC = new Queue<Acceleration>(1000);
        private Queue<AngularVelocity> valuesGYR = new Queue<AngularVelocity>(1000);
        private Queue<Quaternion> valuesQUA = new Queue<Quaternion>(1000);
        private Queue<DateTime> timeACC = new Queue<DateTime>(1000);
        private Queue<DateTime> timeGYR = new Queue<DateTime>(1000);
        private Queue<DateTime> timeQUA = new Queue<DateTime>(1000);

        //private int counter = 0;
        private IAccelerometer acc = null;
        private IGyroBmi160 gyr = null;
        //private IMagnetometerBmm150 mag = null;
        private ISensorFusionBosch sfusion = null;
        private ILogging logger = null;
        private IRoute route = null;

        public Sensor()
        {
            MessStatus = true;
        }

        public Sensor(DeviceRepresentation device)
        {
            try
            {
                initializeBoard(device);
                MessStatus = true;
            }
            catch (ApplicationException exc)
            {
                exceptionPopup.Content = exc.Message;
                exceptionPopup.ShowAsync();
                //TODO
            }

        }
        private async Task initializeBoard(DeviceRepresentation device)
        {
            Device = device;
            try
            {
                var board = MbientLab.MetaWear.Win10.Application.GetMetaWearBoard(device.device);
                metawear = (IMetaWearBoard)board;
                // HIER FINDET DAS VERBINDEN STATT
                await board.InitializeAsync();
                //Thread.Sleep(1000);
                await Task.Delay(1000);
            }
            catch (ApplicationException exc)
            {
                exceptionPopup.Content = exc.Message;
                await exceptionPopup.ShowAsync();
                MessStatus = false;
            }
        }

        internal async Task configure_Modules()
        {
            //metawear = (IMetaWearBoard) MbientLab.MetaWear.Win10.Application.GetMetaWearBoard(Device.device);
            if (AcquisitionMode == "Logging")
            {
                //var lg = ((IMetaWearBoard) metawear).GetModule<ILogging>();
                logger = metawear.GetModule<ILogging>();
                logger.ClearEntries();
            }
            try
            {
                if ((SensorChoosen & SensorMask.ACCELEROMETER) == SensorMask.ACCELEROMETER)
                {
                    acc = metawear.GetModule<IAccelerometer>();
                    // Configure (float odr=100f, float range=2f)
                    // enum  	AccRange { _2g, _4g, _8g, _16g }
                    acc.Configure(odr: 100f, range: 8f);
                    if (AcquisitionMode == "Streaming")
                    {
                        route = await acc.Acceleration.AddRouteAsync(source => source.Stream(data =>
                        {
                            valuesACC.Enqueue(data.Value<Acceleration>());
                            timeACC.Enqueue(data.Timestamp);
                        }));
                    }
                    else
                    {
                        route = await acc.Acceleration.AddRouteAsync(source => source.Log(
                            data =>
                            {
                                //toDo(data);
                                valuesACC.Enqueue(data.Value<Acceleration>());
                                timeACC.Enqueue(data.Timestamp);
                            }
                          ));
                        //route = await metawear.GetModule<IAccelerometer>().Acceleration.AddRouteAsync(source => source.Log());

                    }
                }
                if ((SensorChoosen & SensorMask.GYROSCOPE) == SensorMask.GYROSCOPE)
                {
                    gyr = metawear.GetModule<IGyroBmi160>();
                    // Configure (OutputDataRate odr=OutputDataRate._100Hz, DataRange range=DataRange._125dps, FilterMode filter=FilterMode.Normal)
                    // enum  	GyroRange { _2000dps, _1000dps, _500dps, _250dps }
                    gyr.Configure();
                    if (AcquisitionMode == "Streaming")
                    {
                        route = await gyr.AngularVelocity.AddRouteAsync(source => source.Stream(data =>
                        {
                            valuesGYR.Enqueue(data.Value<AngularVelocity>());
                            timeGYR.Enqueue(data.Timestamp);
                        }));
                    }
                    else
                    {
                        route = await gyr.AngularVelocity.AddRouteAsync(source => source.Log(data =>
                        {
                            valuesGYR.Enqueue(data.Value<AngularVelocity>());
                            timeGYR.Enqueue(data.Timestamp);
                        }));
                    }
                }
                if ((SensorChoosen & SensorMask.QUATERNION) == SensorMask.QUATERNION)
                {
                    sfusion = metawear.GetModule<ISensorFusionBosch>();
                    // Configure (AcquisitionMode AcquisitionMode=AcquisitionMode.Ndof, AccRange ar=AccRange._16g, GyroRange gr=GyroRange._2000dps, object[] accExtra=null, object[] gyroExtra=null)
                    /* 
                    Task<ImuCalibrationData> MbientLab.MetaWear.Core.ISensorFusionBosch.Calibrate(CancellationToken   ct,
                    int     pollingPeriod = 1000, Action < ImuCalibrationState > progress = null)
                    */
                    // enum  	CalibrationAccuracy { Unreliable, LowAccuracy, MediumAccuracy, HighAccuracy }
                    sfusion.Configure();
                    if (AcquisitionMode == "Streaming")
                    {
                        route = await sfusion.Quaternion.AddRouteAsync(source => source.Stream(data =>
                        {
                            valuesQUA.Enqueue(data.Value<Quaternion>());
                            timeQUA.Enqueue(data.Timestamp);
                        }));
                    }
                    else
                    {
                        route = await sfusion.Quaternion.AddRouteAsync(source => source.Log(data =>
                        {
                            valuesQUA.Enqueue(data.Value<Quaternion>());
                            timeQUA.Enqueue(data.Timestamp);
                        }));
                    }
                }
            }
            catch (ApplicationException exc)
            {
                exceptionPopup.Content = exc.Message;
                await exceptionPopup.ShowAsync();
                await reset();
            }
            // Abschluss
            //Thread.Sleep(1000);
            await Task.Delay(1000);
        }

        //private void toDo(IData data)
        //{
        //    valuesACC.Enqueue(data.Value<Acceleration>());
        //    timeACC.Enqueue(data.Timestamp);
        //}

        internal async Task start_Modules()
        {
            try
            {
                if (AcquisitionMode == "Logging") logger.Start(overwrite: true);
                if ((SensorChoosen & SensorMask.ACCELEROMETER) == SensorMask.ACCELEROMETER)
                {
                    acc.Acceleration.Start();
                    acc.Start();
                }
                if ((SensorChoosen & SensorMask.GYROSCOPE) == SensorMask.GYROSCOPE)
                {
                    gyr.AngularVelocity.Start();
                    gyr.Start();
                }
                if ((SensorChoosen & SensorMask.QUATERNION) == SensorMask.QUATERNION)
                {
                    sfusion.Quaternion.Start();
                    sfusion.Start();
                }
            }
            catch (ApplicationException exc)
            {
                exceptionPopup.Content = exc.Message;
                await exceptionPopup.ShowAsync();
                await reset();
            }
        }

        internal async Task stop_Modules()
        {
            try
            {
                if ((SensorChoosen & SensorMask.ACCELEROMETER) == SensorMask.ACCELEROMETER)
                {
                    acc.Acceleration.Stop();
                    acc.Stop();
                }
                if ((SensorChoosen & SensorMask.GYROSCOPE) == SensorMask.GYROSCOPE)
                {
                    gyr.AngularVelocity.Stop();
                    gyr.Stop();
                }
                if ((SensorChoosen & SensorMask.QUATERNION) == SensorMask.QUATERNION)
                {
                    sfusion.Quaternion.Stop();
                    sfusion.Stop();
                }
                if (AcquisitionMode == "Logging")
                {
                    logger.Stop();
                }
            }
            catch (ApplicationException exc)
            {
                exceptionPopup.Content = exc.Message;
                await exceptionPopup.ShowAsync();
                await reset();
            }
        }
        internal async Task downloadData()
        {
            if (AcquisitionMode == "Logging")
            {
                try
                {
                    //metawear.GetModule<ILogging>().Stop();
                    //logger = metawear.GetModule<ILogging>();
                    var settings = metawear.GetModule<ISettings>();
                    settings.EditBleConnParams(maxConnInterval: 7.5f);
                    if (route.Valid)
                        await logger.DownloadAsync();
                    await Task.Delay(1500);
                    //Thread.Sleep(15000);
                    //TODO: Navigation zu Darstellungsseite
                    //await Navigation.PushAsync(new ScannerPage());
                }
                catch (ApplicationException exc)
                {
                    exceptionPopup.Content = exc.Message;
                    await reset();
                    await exceptionPopup.ShowAsync();
                }
            }
        }
        internal async Task reset()
        {
            if (!metawear.InMetaBootMode)
            {
                MessStatus = false;
                metawear.TearDown();
                await metawear.GetModule<IDebug>().DisconnectAsync();
            }
        }

        private String datetime2String(DateTime t)
        {
            return t.ToLongTimeString() + "." + String.Format("{0:000}", t.Millisecond);
        }

        internal String prepareData2write()
        {
            String sb = String.Empty;
            try
            {
                if ((SensorChoosen & SensorMask.ACCELEROMETER) == SensorMask.ACCELEROMETER)
                {
                    DateTime tpoint = timeACC.Peek();
                    sb += ("\nDatum;" + tpoint.ToShortDateString() + "\n");
                    Acceleration datapoint = valuesACC.Peek();
                    sb += ("Time;Acceleration\n");
                    sb += ("T;X;Y;Z\n");
                    //datapoints.Add("Timestamp,X,Y,Z");

                    while (timeACC.TryDequeue(out tpoint) && valuesACC.TryDequeue(out datapoint))
                    //foreach (var datapoint in valuesACC)
                    {
                        sb += (datetime2String(tpoint)); sb += (";");
                        sb += (datapoint.X); sb += (";");
                        sb += (datapoint.Y); sb += (";");
                        sb += (datapoint.Z); sb += ("\n");
                        //sb += (timeACC.Dequeue().ToShortTimeString()); sb += (";");
                        //sb += (valuesACC.Dequeue().X); sb += (";");
                        //sb += (valuesACC.Dequeue().Y); sb += (";");
                        //sb += (valuesACC.Dequeue().Z); sb += ("\n");
                    }
                }
                if ((SensorChoosen & SensorMask.GYROSCOPE) == SensorMask.GYROSCOPE)
                {
                    DateTime tpoint = timeGYR.Peek();
                    sb += ("\nDatum;" + tpoint.ToShortDateString() + "\n");
                    AngularVelocity datapoint = valuesGYR.Peek();
                    sb += ("\nTime;Angular Velocity\n");
                    sb += ("T;X;Y;Z\n");
                    while (timeGYR.TryDequeue(out tpoint) && valuesGYR.TryDequeue(out datapoint))
                    //foreach (var datapoint in valuesACC)
                    {
                        sb += (datetime2String(tpoint)); sb += (";");
                        sb += (datapoint.X); sb += (";");
                        sb += (datapoint.Y); sb += (";");
                        sb += (datapoint.Z); sb += ("\n");
                    }
                }
                if ((SensorChoosen & SensorMask.QUATERNION) == SensorMask.QUATERNION)
                {
                    DateTime tpoint = timeQUA.Peek();
                    sb += ("\nDatum;" + tpoint.ToShortDateString() + "\n");
                    Quaternion datapoint = valuesQUA.Peek();
                    sb += ("\nTime;Quaternion\n");
                    sb += ("T;W;X;Y;Z\n");
                    while (timeQUA.TryDequeue(out tpoint) && valuesQUA.TryDequeue(out datapoint))
                    //foreach (var datapoint in valuesACC)
                    {
                        sb += (datetime2String(tpoint)); sb += (";");
                        sb += (datapoint.W); sb += (";");
                        sb += (datapoint.X); sb += (";");
                        sb += (datapoint.Y); sb += (";");
                        sb += (datapoint.Z); sb += ("\n");
                    }
                }
            }
            catch (Exception exc)
            {
                exceptionPopup.Content = exc.Message;
                exceptionPopup.ShowAsync();
                reset();
            }
            return sb;
        }
    }
}
