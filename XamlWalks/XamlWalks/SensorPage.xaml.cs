﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Windows;
using Windows.Media;
using Windows.UI;
//using MbientLab.MetaWear.Sensor;
//using MbientLab.MetaWear;
//using MbientLab.MetaWear.Core;
//using MbientLab.MetaWear.Data;
using System.Threading;
using Windows.ApplicationModel.Core;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Provider;
using MbientLab.MetaWear.Impl;

namespace XamlWalks

{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet oder zu der innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class SensorPage : Page
    {
        public static SolidColorBrush Green { get; }
        public static SolidColorBrush Gray { get; }

        Brush Colour;
        private DeviceRepresentation device;
        private Sensor sensor = null;

        //private mwbWrapper metawear;
        //private IMetaWearBoard metawear;

        //private List<Acceleration> valuesACC = new List<Acceleration>(1000);
        //private List<AngularVelocity> valuesGYR = new List<AngularVelocity>(1000);
        //private List<Quaternion> valuesQUA = new List<Quaternion>(1000);
        //private int counter = 0;
        //private IAccelerometer acc = null;
        //private IGyroBmi160 gyr = null;
        //private IMagnetometerBmm150 mag = null;
        //private ISensorFusionBosch sfusion = null;

        private ContentDialog initPopup = new ContentDialog()
        {
            Title = "Initializing API",
            Content = "Please wait while the app initializes the API"
        };

        string acquisitionMode = "Streaming";
        public string AcquisitionMode
        {
            get { return sensor.AcquisitionMode; }
            set { sensor.AcquisitionMode = value; }
        }
        private Sensor.SensorStatus sensorStatus = Sensor.SensorStatus.Nichtverbunden;
        public Sensor.SensorStatus SensorStatus
        {
            get { return sensor.Status; }
            set { sensor.Status = value; }
        }
        private Sensor.SensorMask sensorChoosen = Sensor.SensorMask.None;
        public Sensor.SensorMask SensorChoosen
        {
            get { return sensor.SensorChoosen; }
            set { sensor.SensorChoosen = value; }
        }

        public SensorPage()
        {

            this.InitializeComponent();

            Colour = LgTextBlock.Foreground;
            setLabel();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            if (e.Parameter is DeviceRepresentation)
            {
                device = (DeviceRepresentation)e.Parameter;
                if (device != null && device.device != null)
                {
                    initPopup.ShowAsync();
                    sensor = new Sensor(device);
                    //sensor = new Sensor();
                    //sensor.Device = device;
                    //var board = MbientLab.MetaWear.Win10.Application.GetMetaWearBoard(device.device);
                    //// HIER FINDET DAS VERBINDEN STATT
                    //await board.InitializeAsync();

                    StreamingC.IsChecked = true;
                    //AcquisitionMode = "Streaming";
                    CheckAcc.IsChecked = true;
                    //setSensorMask(Sensor.SensorMask.ACCELEROMETER);
                    initPopup.Hide();
                    setButtonsStatus(Sensor.SensorStatus.Verbunden);
                }
                else
                    setButtonsStatus(Sensor.SensorStatus.Nichtverbunden);
            }
        }

        //--Methods
        //set the TextBlocks
        private void setLabel()
        {
            //SensTextA1.Text = reading_Attributes[0];
            //SensTextB1.Text = reading_Attributes[1];
        }

        private async void configure_Click(object sender, RoutedEventArgs e)
        {
            setButtonsStatus(Sensor.SensorStatus.Bereit);
            await sensor.configure_Modules();
            if (!sensor.MessStatus) Frame.GoBack();
            setButtonsStatus(Sensor.SensorStatus.Bereit, false);
        }

        //--Buttons
        //Start Button
        private async void start_Click(object sender, RoutedEventArgs e)
        {
            setButtonsStatus(Sensor.SensorStatus.Gestartet);
            await sensor.start_Modules();
            setButtonsStatus(Sensor.SensorStatus.Gestartet, false);
            if (!sensor.MessStatus) Frame.GoBack();
        }

        private async void stop_Click(object sender, RoutedEventArgs e)
        {
            setButtonsStatus(Sensor.SensorStatus.Gestoppt);
            await sensor.stop_Modules();
            setButtonsStatus(Sensor.SensorStatus.Gestoppt, false);
            if (!sensor.MessStatus) Frame.GoBack();
        }
        private async void download_Click(object sender, RoutedEventArgs e)
        {
            setButtonsStatus(Sensor.SensorStatus.Download);
            await sensor.downloadData();
            write_data_to_CSV();
            setButtonsStatus(Sensor.SensorStatus.Download, false);
            if (!sensor.MessStatus) Frame.GoBack();
        }


        private async void back_Click(object sender, RoutedEventArgs e)
        {
            await sensor.reset();
            setButtonsStatus(Sensor.SensorStatus.Wartend);
            Frame.GoBack();
        }

        public async void write_data_to_CSV(string path = "")  //TODO: Reqork for Xamarin
        {
            if (path == "")
            {
                path = DateTime.Now.ToString("yyyy-MM-ddTHH-mm-ss");
            }

            //foreach (var data in values)
            //{
            if (SensorChoosen != Sensor.SensorMask.None)  // if data Field contains data, do stuff
                                                          //if (data.Value.Count != 0)  // if data Field contains data, do stuff
            {
                string temp_path = path + ".csv"; //"_Accelerator" + 
                                                  //List<string> datapoints = new List<string>();
                String sb = sensor.prepareData2write();
                //string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), temp_path);
                string fileName = @"c:\" + temp_path;
                //////////////////////////////////////////////////////////////////////
                //OutputTextBlock.Text = "";

                FileSavePicker savePicker = new FileSavePicker();
                savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
                // Dropdown of file types the user can save the file as
                savePicker.FileTypeChoices.Add("Plain Text", new List<string>() { ".csv" });
                // Default file name if the user does not type one in or select a file to replace
                savePicker.SuggestedFileName = fileName;
                StorageFile file = await savePicker.PickSaveFileAsync();
                if (file != null)
                {
                    // Prevent updates to the remote version of the file until we finish making changes and call CompleteUpdatesAsync.
                    CachedFileManager.DeferUpdates(file);
                    // write to file
                    await FileIO.WriteTextAsync(file, sb);
                    // Let Windows know that we're finished changing the file so the other app can update the remote version of the file.
                    // Completing updates may require Windows to ask for user input.
                    FileUpdateStatus status = await CachedFileManager.CompleteUpdatesAsync(file);
                    //    if (status == FileUpdateStatus.Complete)
                    //    {
                    //        OutputTextBlock.Text = "File " + file.Name + " was saved.";
                    //    }
                    //    else if (status == FileUpdateStatus.CompleteAndRenamed)
                    //    {
                    //        OutputTextBlock.Text = "File " + file.Name + " was renamed and saved.";
                    //    }
                    //    else
                    //    {
                    //        OutputTextBlock.Text = "File " + file.Name + " couldn't be saved.";
                    //    }
                    //}
                    //else
                    //{
                    //    OutputTextBlock.Text = "Operation cancelled.";
                    //}
                }
                // File.WriteAllText(fileName, string.Join("\n", datapoints));
                //}
            }
            //}
            //return path;
        }


        private void setButtonsStatus(Sensor.SensorStatus status, bool disable = true)
        {
            SensorStatus = status;
            switch (status)
            {
                case Sensor.SensorStatus.Verbunden:
                    btnConfigure.IsEnabled = true;
                    btnBack.IsEnabled = true;
                    break;
                case Sensor.SensorStatus.Bereit:
                    if (disable) btnConfigure.IsEnabled = false;
                    else btnStart.IsEnabled = true;
                    break;
                case Sensor.SensorStatus.Gestartet:
                    if (disable)
                    {
                        btnStart.IsEnabled = false;
                        btnBack.IsEnabled = false;
                    }
                    else btnStop.IsEnabled = true;
                    break;
                case Sensor.SensorStatus.Gestoppt:
                    if (disable) btnStop.IsEnabled = false;
                    else
                    {
                        btnDownload.IsEnabled = true;
                        btnBack.IsEnabled = true;
                    }
                    break;
                case Sensor.SensorStatus.Download:
                    if (disable)
                    {
                        btnDownload.IsEnabled = false;
                        btnBack.IsEnabled = false;
                    }
                    else
                        btnBack.IsEnabled = true;
                    break;
                case Sensor.SensorStatus.Wartend:
                default:
                    btnBack.IsEnabled = true;
                    break;
            }
        }
        private void setSensorMask(Sensor.SensorMask mask)
        {
            if (AcquisitionMode == "Streaming")
                SensorChoosen = mask;
            else
                SensorChoosen |= mask;
        }

        private void optBtn_Checked(CheckBox box, String name)
        {
            if ((bool)box.IsChecked)
            {
                switch (name)
                {
                    case "Acceleration":
                        setSensorMask(Sensor.SensorMask.ACCELEROMETER); break;
                    case "Gyroscope":
                        setSensorMask(Sensor.SensorMask.GYROSCOPE); break;
                    case "Quaternion":
                        setSensorMask(Sensor.SensorMask.QUATERNION); break;
                    case "Euler Angles":
                        setSensorMask(Sensor.SensorMask.EULER_ANGLE); break;
                    case "Linear Acceleration":
                        setSensorMask(Sensor.SensorMask.LINEAR_ACCELERATION); break;
                    case "Gravity":
                        setSensorMask(Sensor.SensorMask.GRAVITY); break;
                    case "Magnetometer":
                        setSensorMask(Sensor.SensorMask.MAGNETOMETER); break;
                }
                setLabel();
            }
        }

        //--Checkboxes 
        //EulerBox
        private void EulerC_Checked(object sender, RoutedEventArgs e)
        {
            optBtn_Checked((CheckBox)sender, "Euler Angles");
        }

        //Linear Acceleration Checkbox
        private void LinAccC_Checked(object sender, RoutedEventArgs e)
        {
            optBtn_Checked((CheckBox)sender, "Linear Acceleration");
        }

        //Gravity Checkbox
        private void GravityC_Checked(object sender, RoutedEventArgs e)
        {
            optBtn_Checked((CheckBox)sender, "Gravity");
        }

        //Acceleration Checkbox
        private void CheckAcc_Checked(object sender, RoutedEventArgs e)
        {
            optBtn_Checked((CheckBox)sender, "Acceleration");
        }

        //Gyroscope Checkbox
        private void CheckGyro_Checked(object sender, RoutedEventArgs e)
        {
            optBtn_Checked((CheckBox)sender, "Gyroscope");
        }

        //Quaternion Checkbox
        private void QuatC_Checked(object sender, RoutedEventArgs e)
        {
            optBtn_Checked((CheckBox)sender, "Quaternion");
        }

        //Magnetometer Checkbox
        private void CheckMagn_Checked(object sender, RoutedEventArgs e)
        {
            optBtn_Checked((CheckBox)sender, "Magnetometer");

        }

        //--Setting the AcquisitionMode 
        //logging
        private void LoggingC_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)LoggingC.IsChecked)
            {
                StreamingC.IsChecked = false;
                AcquisitionMode = "Logging";
            }
            else
            {
                StreamingC.IsChecked = true;
                AcquisitionMode = "Streaming";
            }
        }

        //streaming
        private void StreamingC_Checked(object sender, RoutedEventArgs e)
        {
            if ((bool)StreamingC.IsChecked)
            {
                LoggingC.IsChecked = false;
                AcquisitionMode = "Streaming";
            }
            else
            {
                LoggingC.IsChecked = true;
                AcquisitionMode = "Logging";
            }
        }

    }
}
